package com.parkingwang.learning.hello;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public class HelloWold {

    /**
     * subscribe()
     * 多个重载方法
     * subscribe(onNext)
     * subscribe(onNext,onError)
     * subscribe(onNext,onError,onComplete)
     * subscribe(onNext,onError,onComplete,unSubscribe)
     *
     * Observable  被观察者
     * Observer  观察者
     * subscribe 纽带 ，订阅
     *
     *
     * @param args
     */
    public static void main(String[] args){


//        printHello1();

        //Rxjava2不再支持subscribe,使用observer替代
        printHello2();


    }

    private static void printHello2() {
        Observable.just("HelloWorld")
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String s) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private static void printHello1() {
        Observable.just("HelloWorld")
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        System.out.println(s);  //2
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        System.out.println(throwable.toString());  //2
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {
                        System.out.println("onComplete");  //3
                    }
                }, new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        System.out.println("subscribe"); //1
                    }
                });
    }

}
