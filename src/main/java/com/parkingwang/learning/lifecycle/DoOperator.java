package com.parkingwang.learning.lifecycle;

import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public class DoOperator {

    public static void main(String[] args) {


        Observable.just("HelloDo1", "HelloDo2", "HelloDo3")
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        //1
                        //观察者一旦订阅Observable
                        System.out.println("doOnSubscribe");
                    }
                })
                .doOnLifecycle(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        //2
                        //方法内部可以在订阅后取消订阅
                        System.out.println("doOnLifecycle-----" + disposable.isDisposed());
                    }
                }, new Action() {
                    @Override
                    public void run() throws Exception {

                        System.out.println("doOnLifecycle---run");
                    }
                })
                .doOnNext(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        //3
                        //Observable 每发射一项数据就会调用一次,onNext之前
                        System.out.println("doOnNext---" + s);
                    }
                })
                .doOnEach(new Consumer<Notification<String>>() {
                    @Override
                    public void accept(Notification<String> stringNotification) throws Exception {
                        //4  8
                        //Observable 每发射一项数据就会调用一次,包括next,error,complete
                        System.out.println("doOnEach-----" + (

                                stringNotification.isOnNext() ? "next"
                                        : stringNotification.isOnComplete() ? "complete"
                                        : "error"
                        ));
                    }
                })
                .doAfterNext(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        //6
                        //onNext之后
                        System.out.println("doAfterNext----" + s);
                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {
                        //7
                        //等到数据发送完毕才调用
                        System.out.println("doOnComplete");
                    }
                })

                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        //9   complete,error 都会触发
                        System.out.println("doFinally");
                    }
                })

                .doAfterTerminate(new Action() {
                    @Override
                    public void run() throws Exception {
                        //10  complete,error 都会触发
                        System.out.println("doAfterTerminate");
                    }
                })

                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        //5
                        System.out.println("收到消息：" + s);
                    }
                });


    }

}
