package com.parkingwang.learning.observertype;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public class CompletableRx {

    /**
     * 只有complete error
     * 没有map,flatmap操作符
     *
     * 可以和andThen配套使用
     *
     * @param args
     */
    public static void main(String[] args) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                System.out.println("hello");
            }
        }).andThen(Observable.range(1,10)) //range 操作符背压
        .subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                System.out.println(integer);
            }
        });

    }
}
