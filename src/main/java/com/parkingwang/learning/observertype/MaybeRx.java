package com.parkingwang.learning.observertype;

import io.reactivex.Maybe;
import io.reactivex.MaybeEmitter;
import io.reactivex.MaybeOnSubscribe;
import io.reactivex.functions.Consumer;

public class MaybeRx {


    /**
     * 只能发送0或1个数据
     *
     * @param args
     */
    public static void main(String[] args) {
        Maybe.create(new MaybeOnSubscribe<String>() {
            @Override
            public void subscribe(MaybeEmitter<String> emitter) throws Exception {
                     emitter.onSuccess("maybe");
                emitter.onSuccess("maybe---next");
            }
        }).subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {
                System.out.println(s);
            }
        });
    }

}
