package com.parkingwang.learning.observertype;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;

public class SingleRx {

    /**
     *
     * Single 只能发射一个数据， 成功后回调在onSuccess
     *
     * 并没有complete()
     *
     * @param args
     */
    public static void main(String[] args) {

        Single.create(new SingleOnSubscribe<String>() {
            @Override
            public void subscribe(SingleEmitter<String> emitter) throws Exception {
//                emitter.onSuccess("single");
//                emitter.onSuccess("single===next");
                int i = 1 / 0;
            }
        })
//                .subscribe(new Consumer<String>() {
//            @Override
//            public void accept(String s) throws Exception {
//                System.out.println(s);
//            }
//        }, new Consumer<Throwable>() {
//            @Override
//            public void accept(Throwable throwable) throws Exception {
//                System.out.println(throwable.toString());
//            }
//        });

                .subscribe(new BiConsumer<String, Throwable>() {
                    @Override
                    public void accept(String s, Throwable throwable) throws Exception {
                        System.out.println(throwable == null ? s : throwable.toString());
                    }
                }); //简写


//        toXXXX  可以转其他四种方式，但只能还是只能发射一次？？？

        Single<Integer> single = Single.create(new SingleOnSubscribe<Integer>() {
            @Override
            public void subscribe(SingleEmitter<Integer> emitter) throws Exception {
                emitter.onSuccess(1);
                emitter.onSuccess(2);
            }
        });

        single.toObservable().subscribe(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) throws Exception {
                System.out.println(integer);
            }
        });



    }

}
