package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func0;

public class DeferDemo {

    public static void main(String[] args) {

        //defer创建的被观察者实际没什么用，在订阅的时候，内部会创建并发送一个新的被观察者与订阅的观察者关联
        //每关联一个观察者对象，内部就会创建一个新的被观察者关联订阅的观察者
        Observable<String> observable = Observable.defer(new Func0<Observable<String>>() {
            @Override
            public Observable<String> call() {
                return Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        if (subscriber.isUnsubscribed()) {
                            return;
                        }
                        subscriber.onNext("hello");
                    }
                });
            }
        });

        observable.subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                System.out.println("next1" + s);
            }
        });

        observable.subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                System.out.println("next2" + s);
            }
        });

    }

}
