package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

public class DistinctFilterDemo {

    public static void main(String[] args) {


        Observable.from(new Person[]{
                new Person("小明", 18),
                new Person("小红", 16),
                new Person("小明", 15),
                new Person("小强", 20),
        }).distinct(new Func1<Person, String>() {
            @Override
            public String call(Person person) {
                //过滤名字重复的Person
                return person.name;
            }
        }).subscribe(new Action1<Person>() {
            @Override
            public void call(Person person) {
                System.out.println(person);
            }
        });


        Observable.from(new Person[]{
                new Person("小明", 18),
                new Person("小红", 16),
                new Person("小明", 15),
                new Person("小强", 20),
        }).filter(new Func1<Person, Boolean>() {
            @Override
            public Boolean call(Person person) {
                //过滤名字重复的Person
                return person.age > 18;
            }
        }).subscribe(new Action1<Person>() {
            @Override
            public void call(Person person) {
                System.out.println(person);
            }
        });

    }

}
