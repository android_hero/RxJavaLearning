package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;

import java.util.concurrent.TimeUnit;

public class DoDemo {

    /**
     * Do 对流程步骤的监听
     * 可以监听onNext()和subscribe()
     * 在每个回调之前监听到
     * doOnEach()、doOnNext()，doOnSubscribe()
     *
     * @param args
     */
    public static void main(String[] args) {

        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("hello");
                subscriber.onCompleted();
            }
        })
//                .doOnEach(new Observer<String>() {
//                    @Override
//                    public void onCompleted() {
//                        System.out.println("doOnCompleted");
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        System.out.println("doOnError");
//                    }
//
//                    @Override
//                    public void onNext(String s) {
//                        System.out.println("doOnNext");
//                    }
//                })
//                .doOnNext(new Action1<String>() {
//                    @Override
//                    public void call(String s) {
//                        System.out.println("doOnNext===" + s);
//                    }
//                })
                //延迟订阅，但不影响开始订阅doOnSubscribe回调的执行
//                .delaySubscription(5,TimeUnit.SECONDS)
                //开始订阅就监听到了
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        System.out.println("doOnSubscribe");
                    }
                })
                .subscribe(new Observer<String>() {
                    @Override
                    public void onCompleted() {
                        System.out.println("onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("onError" + e.getMessage());
                    }

                    @Override
                    public void onNext(String s) {
                        System.out.println("onNext---" + s);
                    }
                });

    }
}
