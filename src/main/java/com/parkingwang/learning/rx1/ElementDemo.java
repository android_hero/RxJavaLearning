package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;

public class ElementDemo {

    public static void main(String[] args) {

        Observable.from(new Person[]{
                new Person("小明", 18),
                new Person("小红", 16),
                new Person("小明", 15),
                new Person("小强", 20),
        })
//                .first()
//                .last()
                .elementAt(1)
                .subscribe(new Action1<Person>() {
                    @Override
                    public void call(Person person) {
                        System.out.println(person);
                    }
                });

        Observable.from(new Person[]{
                new Person("小明", 18),
                new Person("小红", 16),
                new Person("小明", 15),
                new Person("小强", 20),
        })
//                .take(2)
                .takeLast(3)
                .subscribe(new Action1<Person>() {
                    @Override
                    public void call(Person person) {
                        System.out.println(person);
                    }
                });

        //不接收发射的数据，直接调用onComplete()
        Observable.from(new Person[]{
                new Person("小明", 18),
                new Person("小红", 16),
                new Person("小明", 15),
                new Person("小强", 20),
        })
                .ignoreElements()
                .subscribe(new Action1<Person>() {
                    @Override
                    public void call(Person person) {
                        System.out.println(person);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }, new Action0() {
                    @Override
                    public void call() {
                        System.out.println("onComplete");
                    }
                });




    }

}
