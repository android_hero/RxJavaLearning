package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;

public class ExceptionDemo {

    public static void main(String[] args) {

        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("hello");
                subscriber.onError(new NullPointerException("空指针异常"));
                subscriber.onNext("world");

            }
        })
//                .onErrorReturn(new Func1<Throwable, String>() {
//                    @Override
//                    public String call(Throwable throwable) {
//                        return "发生异常了--" + throwable.getMessage();
//                    }
//                })
//                .onErrorResumeNext(Observable.just("发生异常了"))
                .onExceptionResumeNext(Observable.just("发生异常了!"))
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println(s);
                    }
                });

    }


}
