package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

public class IntervalTimerDemo {

    public static void main(String[] args) {

        System.out.println(Thread.currentThread().getName());

        //Java的定时器
//        new Timer().schedule(new TimerTask() {
//            @Override
//            public void run() {
//                System.out.println(Thread.currentThread().getName());
//            }
//        }, 3000, 3000);

//        Observable.interval(2, TimeUnit.SECONDS, Schedulers.io())
//                .subscribe(new Action1<Long>() {
//                    @Override
//                    public void call(Long aLong) {
//                        //从0开始计数
//                        System.out.println(Thread.currentThread().getName() + "---interval---" + aLong);
//                    }
//                });

        //推荐使用interval()
//        Observable.timer(2, 2, TimeUnit.SECONDS, Schedulers.io())
//                .subscribe(new Action1<Long>() {
//                    @Override
//                    public void call(Long aLong) {
//                        System.out.println(Thread.currentThread().getName() + "---timer---" + aLong);
//                    }
//                });

        //delay 延迟3秒操作
        Observable.just("world")
                .delay(3,TimeUnit.SECONDS)
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println(Thread.currentThread().getName() + "---delay---" + s);
                    }
                });


        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
