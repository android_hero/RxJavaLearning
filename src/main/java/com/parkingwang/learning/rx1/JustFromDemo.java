package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.functions.Action1;

public class JustFromDemo {

    public static void main(String[] args) {

        //最多发射9个数据,成功会自动回调onComplete()
        Observable.just("A")
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println(s);
                    }
                });

        //发射Iterable子类或者数组,成功会自动回调onComplete()
        Observable.from(new Integer[]{1, 2, 3})
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println(integer);
                    }
                });

    }
}
