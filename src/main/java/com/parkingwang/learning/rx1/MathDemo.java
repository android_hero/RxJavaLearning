package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.functions.Action1;
import rx.observables.MathObservable;

public class MathDemo {

    public static void main(String[] args) {

        Observable<Integer> observable = Observable.just(1, 2, 3, 4, 5);
        MathObservable.averageInteger(observable)
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println(integer);
                    }
                });
        MathObservable.min(observable)
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println(integer);
                    }
                });
        MathObservable.max(observable)
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println(integer);
                    }
                });
        MathObservable.sumInteger(observable)
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println(integer);
                    }
                });

        //rxjava1自带求数据个数
        observable.count()
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println(integer);
                    }
                });


    }
}
