package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;

public class OperatorDemo {

    public static void main(String[] args) {

//        all();

//        amb();

//        contains();

//        defaultIfEmpty();

//        sequenceEqual();

//        takeSkipWhile();


    }

    private static void takeSkipWhile() {
        Observable.just(1, 2, 3, 4, 5)
                .takeWhile(new Func1<Integer, Boolean>() {
                    @Override
                    public Boolean call(Integer integer) {
//                        skipWhile 直到条件不满足才发射数据
                        //takeWhile  直到条件不满足就不发射数据
                        return integer != 4;
                    }
                })
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println(integer);
                    }
                });
    }

    private static void sequenceEqual() {
        Observable<Integer> networkObservable = Observable.just(1, 2, 3, 4);
        Observable<Integer> localObservable = Observable.just(1, 2, 3, 4);

        //比较网络和本地存储的数据是否相同，排列顺序不同也是false
        Observable.sequenceEqual(networkObservable, localObservable)
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        System.out.println(aBoolean);
                    }
                });
    }

    private static void defaultIfEmpty() {
        //没有发射任何数据使用默认的数据
        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
//                subscriber.onNext("hello");
                subscriber.onCompleted();
            }
        }).defaultIfEmpty("default data")
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println(s);
                    }
                });
    }

    private static void contains() {
        Observable.just("hello", "world", "java")
                .contains("java1")
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        System.out.println(aBoolean);
                    }
                });
    }

    private static void amb() {
        //amb抢占,前面观察者占用了，后面将不再发射
        Observable<String> helloObservable = Observable.just("hello");
        Observable<String> worldObservable = Observable.just("world");

        Observable.amb(helloObservable, worldObservable)
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println(s);
                    }
                });
    }

    private static void all() {

        //筛选数据必须全部符合条件才会是true，否则为false
        Observable.from(new Person[]{
                new Person("小明", 18),
                new Person("小红", 16),
                new Person("小明", 15),
                new Person("小强", 20),
        })
                .all(new Func1<Person, Boolean>() {
                    @Override
                    public Boolean call(Person person) {
                        return person.age > 14;
                    }
                })
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        System.out.println(aBoolean);
                    }
                });
    }
}
