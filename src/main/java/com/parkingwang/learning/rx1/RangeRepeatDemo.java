package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class RangeRepeatDemo {

    public static void main(String[] args) {

        //
        Observable.range(3, 20)
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        System.out.println("---" + integer);
                    }
                });

        //重复发送3遍
        Observable.just("hello")
                .repeat(3)
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println("---" + s);
                    }
                });


    }

}
