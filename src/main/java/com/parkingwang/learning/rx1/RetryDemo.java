package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;

public class RetryDemo {

    public static void main(String[] args) {

        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("hello");
                subscriber.onError(new NullPointerException("空指针异常"));
                subscriber.onNext("world");

            }
        })
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        //重试每次都会重新回调一次这个方法
                        System.out.println("doOnSubscribe---");
                    }
                })
                .retry(3)//如果发生异常会先重试发射数据，
                // 不指定重试次数会无限制重试下去，最大重试次数是N，总共最大操作次数N+1
                .onErrorResumeNext(Observable.empty())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println(s);
                    }
                });
    }
}
