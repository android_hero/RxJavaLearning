package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Action0;

public class SerializeDemo {

    public static void main(String[] args) {

        //默认调用onComplete()之后就会取消订阅
        //unsafeSubscribe()不安全订阅会，调用onComplete()后还会继续发射数据，并且不取消订阅，serialize()后一旦调用onComplete()，不会继续发射数据，但并没有取消订阅

        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("A");
                subscriber.onNext("B");
                subscriber.onCompleted();
                subscriber.onNext("C");
                subscriber.onCompleted();
            }
        })
                .serialize()
                .doOnUnsubscribe(new Action0() {
                    @Override
                    public void call() {
                        System.out.println("doOnUnsubscribe");
                    }
                })
// .subscribe(new Observer<String>() {
//            @Override
//            public void onCompleted() {
//                System.out.println("onCompleted");
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                System.out.println("onError" + e.getMessage());
//            }
//
//            @Override
//            public void onNext(String s) {
//                System.out.println("onNext---" + s);
//            }
//        });
                .unsafeSubscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        System.out.println("onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("onError" + e.getMessage());
                    }

                    @Override
                    public void onNext(String s) {
                        System.out.println("onNext---" + s);
                    }
                });

    }
}
