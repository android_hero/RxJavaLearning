package com.parkingwang.learning.rx1;

import rx.Observer;
import rx.Single;
import rx.SingleSubscriber;


//rxjava1 包名rx.xxx  rxjava2 包名io.reactivex.xxx

/**
 * 被观察者 在创建之后就马上发送数据---热Observable   Subject子类
 * 被观察者 在订阅时才发送数据 ----冷Observable    普通Observable
 * 被观察者 在指定时间点发送数据  -----冷Observable    可连接Observable
 *
 */
public class SingleDemo {

    //被观察者只能调用onSuccess()/onError() 并且只能调用一次
    public static void main(String[] args) {
        Single<Integer> single = Single.create(new Single.OnSubscribe<Integer>() {
            @Override
            public void call(SingleSubscriber<? super Integer> singleSubscriber) {
                //onSuccess()会同时回调onNext()和onComplete()方法
                singleSubscriber.onSuccess(10086);
                singleSubscriber.onSuccess(1024);
//                singleSubscriber.onError(new NullPointerException());
            }
        });

        Observer<Integer> observer = new Observer<Integer>() {
            @Override
            public void onCompleted() {
                System.out.println("onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                System.out.println("onError----" + e);
            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("onNext----" + integer);
            }
        };

        single.subscribe(observer);

    }

}
