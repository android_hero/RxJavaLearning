package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;

/**
 * 观察者变体
 */
public class SubscriberDemo {

    public static void main(String[] args) {

        //Subscriber implements Observer<T>, Subscription ,而Subscription提供了取消订阅的方法
//        Observable.create(new Observable.OnSubscribe<String>() {
//            @Override
//            public void call(Subscriber<? super String> subscriber) {
//
//            }
//        }).subscribe(new Subscriber<String>() {
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onError(Throwable e) {
//
//            }
//
//            @Override
//            public void onNext(String s) {
//
//            }
//        });

        //onComplete和onError只能回调其中一个方法
        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("hello world");
                subscriber.onError(new NullPointerException());
                subscriber.onCompleted();
            }
        }).subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                System.out.println("onNext----" + s);
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                System.out.println("onError----" + throwable);
            }
        }, new Action0() {
            @Override
            public void call() {
                System.out.println("onComplete----");
            }
        });


    }

}
