package com.parkingwang.learning.rx1;

import rx.Scheduler;
import rx.functions.Action0;
import rx.schedulers.Schedulers;

public class ThreadDemo {

    public static void main(String[] args) {

        //自己定义一个调度器
        Scheduler.Worker worker = Schedulers.io().createWorker();
        //worker相当于runnable
        worker.schedule(new Action0() {
            @Override
            public void call() {
                System.out.println(Thread.currentThread().getName());
            }
        });
    }
}
