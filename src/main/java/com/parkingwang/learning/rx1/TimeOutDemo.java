package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.functions.Action1;

import java.util.concurrent.TimeUnit;

public class TimeOutDemo {

    public static void main(String[] args) {

        Observable.interval(2, TimeUnit.SECONDS)
                //一般用在网络超时情况
                .timeout(2, TimeUnit.SECONDS)//超时没有发射数据就报java.util.concurrent.TimeoutException
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        System.out.println("onNext---" + aLong);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        System.out.println("onError---" + throwable.toString());
                    }
                });

        try {
            Thread.sleep(100 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
