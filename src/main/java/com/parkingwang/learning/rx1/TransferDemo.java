package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.functions.Action1;

import java.util.List;

public class TransferDemo {

    public static void main(String[] args) {


        buffer_window();

    }

    //被观察者与观察者可能存在不同线程中
    //有时被观察者发送数据较快，观察者来不及处理就被抛弃了，使用buffer或者window可减少此类事件发生
    private static void buffer_window() {
        Observable.just("hello", "world", "java")
                .buffer(2)
                .subscribe(new Action1<List<String>>() {
                    @Override
                    public void call(List<String> strings) {
                        System.out.println("---------call");
                        for (String s : strings) {
                            System.out.println(s);
                        }
                    }
                });

        Observable.just("android", "kotlin", "flutter")
                .window(2)
                .subscribe(new Action1<Observable<String>>() {
                    @Override
                    public void call(Observable<String> stringObservable) {
                        System.out.println("---------call");
                        stringObservable.subscribe(new Action1<String>() {
                            @Override
                            public void call(String s) {
                                System.out.println(s);
                            }
                        });
                    }
                });
    }


}
