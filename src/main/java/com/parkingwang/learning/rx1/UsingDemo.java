package com.parkingwang.learning.rx1;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class UsingDemo {

    //using() 用来创建一个一次性资源的函数，订阅流程结束后就被销毁
    public static void main(String[] args) {

        Observable.using(new Func0<String>() {
            @Override
            public String call() {
                System.out.println(Thread.currentThread().getName() + "-----创建一次性资源");
                return "hello";
            }
        }, new Func1<String, Observable<String>>() {
            @Override
            public Observable<String> call(String s) {
                System.out.println(Thread.currentThread().getName() + "-----发射数据：" + s);
                return Observable.just(s);
            }
        }, new Action1<String>() {
            @Override
            public void call(String s) {
                System.out.println(Thread.currentThread().getName() + "-----一次性资源被销毁了:" + s);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println(Thread.currentThread().getName() + "-----接收到数据：" + s);
                    }
                });

        try {
            Thread.sleep(10 * 1000);
        } catch (InterruptedException ignored) {
        }

    }
}
