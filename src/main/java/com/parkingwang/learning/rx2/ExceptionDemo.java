package com.parkingwang.learning.rx2;


import io.reactivex.*;
import io.reactivex.functions.BiPredicate;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

public class ExceptionDemo {

    public static void main(String[] args) {

        Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> subscriber) throws Exception {
                subscriber.onNext("hello");
//                subscriber.onError(new NullPointerException("空指针异常"));
//                subscriber.onNext("world");
                throw new NullPointerException("空指针异常");
            }
        })
//                .onErrorReturn(new Function<Throwable, String>() {
//                    @Override
//                    public String apply(Throwable throwable) throws Exception {
//                        return "发生异常了:" + throwable.toString();
//                    }
//                })
//                .onErrorResumeNext(new ObservableSource<String>() {
//                    @Override
//                    public void subscribe(Observer<? super String> observer) {
//                        observer.onNext("发生异常了，提醒一次");
//                        observer.onNext("发生异常了，提醒两次");
//                        observer.onNext("发生异常了，提醒N次");
//                    }
//                })
                .onExceptionResumeNext(new ObservableSource<String>() {
                    @Override
                    public void subscribe(Observer<? super String> observer) {
                        observer.onNext("发生异常了，提醒一次");
                        observer.onNext("发生异常了，提醒两次");
                        observer.onNext("发生异常了，提醒N次");
                    }
                })
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        System.out.println(s);
                    }
                });

        Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> subscriber) throws Exception {
                subscriber.onNext("world");
                throw new NullPointerException("空指针异常");
            }
        })
//                .retry()//可以指定重试的次数，不指定就是无限重试
//                .retry(3, new Predicate<Throwable>() {
//                    @Override
//                    public boolean test(Throwable throwable) throws Exception {
//
//                        return true;//false表示不重试,true表示要重试
//                    }
//                })
                .retry(new BiPredicate<Integer, Throwable>() {
                    @Override
                    public boolean test(Integer integer, Throwable throwable) throws Exception {
                        System.out.println("重试次数：" + integer + "，异常信息：" + throwable.toString());
                        return true;//false表示不重试,true表示要重试
                    }
                })
                .onErrorReturn(new Function<Throwable, String>() {
                    @Override
                    public String apply(Throwable throwable) throws Exception {
                        return "发生异常了:" + throwable.toString();
                    }
                })
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        System.out.println(s);
                    }
                });

    }
}
